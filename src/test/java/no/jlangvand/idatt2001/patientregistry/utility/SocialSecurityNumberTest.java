package no.jlangvand.idatt2001.patientregistry.utility;

import org.junit.jupiter.api.Test;

import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class SocialSecurityNumberTest {

  private static final Logger LOGGER =
      Logger.getLogger(SocialSecurityNumber.class.getName());

  @Test
  void validate() {
    try {
      assertTrue(SocialSecurityNumber.validate("13129057224"));
      assertThrows(NumberFormatException.class,
          () -> SocialSecurityNumber.validate("13129012346"));
      assertThrows(NumberFormatException.class,
          () -> SocialSecurityNumber.validate("12139912345"));
    } catch (Exception e) {
      fail();
    }
  }

  @Test
  void generateValidNumber() {
    var number = SocialSecurityNumber.generateValidNumber("131290");
    LOGGER.log(INFO, () -> "Generated " + number);
    assertTrue(SocialSecurityNumber.validate(number));
  }

}