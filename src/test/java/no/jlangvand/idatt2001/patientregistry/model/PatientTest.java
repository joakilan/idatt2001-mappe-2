package no.jlangvand.idatt2001.patientregistry.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PatientTest {

  @Test
  void setSocialSecurityNumber() {
    var patient = new Patient();
    assertThrows(IllegalArgumentException.class,
        () -> patient.setSocialSecurityNumber("123"));
    assertThrows(IllegalArgumentException.class,
        () -> patient.setSocialSecurityNumber("abc"));
    assertThrows(IllegalArgumentException.class,
        () -> patient.setSocialSecurityNumber("aaaaaaaaaaa"));
    assertDoesNotThrow(
        () -> patient.setSocialSecurityNumber("12345678901"));
  }
}