package no.jlangvand.idatt2001.patientregistry.csv;

import org.jetbrains.annotations.NotNull;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.FINE;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

/**
 * Provides methods to serialise or deserialise an object to/from a CSV file.
 *
 * <p>The bound class must declare getters and setters for the fields to
 * serialise/deserialise. It must also declare a constructor with no
 * parameters.
 *
 * @param <T> type of object to relate to a CSV file
 */
public class CSV<T extends Validatable> {

  private static final java.util.logging.Logger LOGGER =
      Logger.getLogger(CSV.class.getName());

  private final String separator;
  private final String filePath;
  private final Class<T> typeParameterClass;

  /**
   * Create a new CSV relation.
   *
   * @param filePath           path to CSV file
   * @param typeParameterClass class of objects ro serialise/deserialise
   * @param separator          string to use as separator
   */
  public CSV(String filePath, Class<T> typeParameterClass, String separator) {
    this.typeParameterClass = typeParameterClass;
    this.filePath = filePath;
    this.separator = separator;
  }

  /**
   * Create a new CSV relation.
   *
   * @param filePath           path to CSV file
   * @param typeParameterClass class of objects ro serialise/deserialise
   */
  public CSV(String filePath, Class<T> typeParameterClass) {
    this(filePath, typeParameterClass, ",");
  }

  private void setField(T object, String key, String value) throws IllegalAccessException {
    try {
      new PropertyDescriptor(key, typeParameterClass).getWriteMethod()
          .invoke(object, value);
    } catch (InvocationTargetException | IntrospectionException e) {
      LOGGER.log(WARNING, () ->
          "Failed to parse field %s: %s".formatted(key, value));
    }
  }

  private void validateAndAddObject(List<T> list, T object) {
    try {
      object.validate();
      list.add(object);
    } catch (Exception e) {
      LOGGER.log(SEVERE, () -> "Validation failed: %s, object is ignored"
          .formatted(e.getMessage()));
    }
  }

  /**
   * Deserialise CSV into an ArrayList.
   *
   * @return list of objects from CSV
   * @throws IOException                  thrown if file IO fails
   * @throws ReflectiveOperationException thrown when reflective operation fails
   *                                      (bad field names, etc)
   */
  @SuppressWarnings("unchecked")
  public List<T> read() throws IOException, ReflectiveOperationException {
    var list = new ArrayList<T>();
    try (var bufferedReader = new BufferedReader(new FileReader(filePath))) {
      var fieldNames = bufferedReader.readLine().split(separator);

      Constructor<T> constructor = null;
      for (var c : typeParameterClass.getDeclaredConstructors()) {
        if (c.getGenericParameterTypes().length == 0) {
          constructor = (Constructor<T>) c;
        }
      }

      if (constructor == null) {
        throw new NullPointerException(
            "Bound class must declare a constructor with no parameters");
      }

      for (var line = ""; (line = bufferedReader.readLine()) != null; ) {
        var object = constructor.newInstance();
        var row = line.split(separator);
        for (var i = 0; i < fieldNames.length; i++) {
          var key = fieldNames[i];
          var value = row[i].equals("null") ? "" : row[i];
          LOGGER.log(FINE, () -> "Invoking setter for %s with %s%n"
              .formatted(key, value));
          setField(object, key, value);
        }
        validateAndAddObject(list, object);
      }
    } catch (IOException e) {
      LOGGER.log(SEVERE, () -> "CSV::read: %s".formatted(e.toString()));
      throw new IOException("Failed to open file, check log for details.");
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      LOGGER.log(SEVERE, () -> "Failed to call method or constructor: %s"
          .formatted(e.toString()));
      throw new ReflectiveOperationException(
          "Failed to read CSV file. File may be corrupt.");
    }

    return list;
  }

  /**
   * Write list of objects to CSV file.
   *
   * @param objects list of objects to write
   * @param fields  name of fields to write
   * @throws IOException
   * @throws IntrospectionException
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  public void write(@NotNull List<T> objects, String... fields) throws IOException, IntrospectionException, InvocationTargetException, IllegalAccessException {
    try (var bufferedWriter = new BufferedWriter(new FileWriter(filePath))) {
      bufferedWriter.write(String.join(separator, fields));
      bufferedWriter.newLine();
      for (var object : objects) {
        var values = new ArrayList<String>();
        for (var field : fields) {
          var value = "%s".formatted(new PropertyDescriptor(field,
              typeParameterClass).getReadMethod().invoke(object));
          values.add(value.isEmpty() ? "null" : value);
        }
        bufferedWriter.write("%s%n".formatted(String.join(separator, values)));
      }
    }
  }

  public String getSeparator() {
    return separator;
  }

  public String getFilePath() {
    return filePath;
  }

}
