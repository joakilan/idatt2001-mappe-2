package no.jlangvand.idatt2001.patientregistry.csv;

public interface Validatable {
  boolean validate();
}
