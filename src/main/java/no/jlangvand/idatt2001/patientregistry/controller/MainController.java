package no.jlangvand.idatt2001.patientregistry.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import no.jlangvand.idatt2001.patientregistry.csv.CSV;
import no.jlangvand.idatt2001.patientregistry.factory.DialogFactory;
import no.jlangvand.idatt2001.patientregistry.model.Patient;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

/**
 * Main application logic.
 */
public class MainController {

  private static final Logger LOGGER =
      Logger.getLogger(MainController.class.getName());

  private final ObservableList<Patient> patients =
      FXCollections.observableArrayList();
  private TableView<Patient> patientTableView;
  private final SimpleStringProperty statusProperty;
  private final Stage primaryStage;

  /** Ordered List of human readable property names */
  public static final List<String> PATIENT_FIELDS;

  // Initialise static fields
  static {
    PATIENT_FIELDS = List.of("Social Security Number", "First Name",
        "Last Name", "General Practitioner", "Diagnosis");
  }

  /**
   * Construct ocntroller belonging to the given stage.
   *
   * @param primaryStage main Stage
   */
  public MainController(Stage primaryStage) {
    this.primaryStage = primaryStage;
    statusProperty = new SimpleStringProperty();
    statusProperty.set("Ready");
  }

  /**
   * Open a CSV file and attempt to import the data.
   *
   * @param file path to CSV file
   */
  public void openFile(File file) {
    try {
      readFromCSV(file.getPath());
    } catch (ReflectiveOperationException e) {
      DialogFactory.alert("Invalid file", Alert.AlertType.ERROR,
          "Chosen file is not a CSV file or it contains invalid fields");
    } catch (IOException e) {
      DialogFactory.alert("Failed to read file", Alert.AlertType.ERROR,
          e.getMessage());
    } catch (NullPointerException e) {
      statusProperty.set("Import cancelled");
    }
  }

  /**
   * Export data to CSV file
   *
   * @param file path to write to
   * @throws IntrospectionException reflection error
   * @throws IOException file IO failed
   * @throws InvocationTargetException reflection error
   * @throws IllegalAccessException reflection error
   */
  public void exportCSV(File file) throws IntrospectionException, IOException, InvocationTargetException, IllegalAccessException {
    try {
      writeToCSV(file.getPath());
    } catch (NullPointerException e) {
      statusProperty.set("Export cancelled");
    }
  }

  /**
   * Show the main stage.
   */
  public void start() {
    primaryStage.show();
  }

  /**
   * Read from CSV file.
   *
   * @param path path to read from
   * @throws ReflectiveOperationException invalid fields in CSV file
   * @throws IOException file IO fails
   */
  public void readFromCSV(String path) throws ReflectiveOperationException, IOException {
    var size = patients.size();
    patients.addAll(new CSV<>(path, Patient.class, ";").read());
    var status = "Imported %d patients".formatted(patients.size() - size);
    statusProperty.set(status);
    DialogFactory.alert("Import complete", status);
  }

  /**
   * Write data to CSV file.
   *
   * @param path path to write to
   * @throws IntrospectionException reflection error
   * @throws IOException file IO fails
   * @throws InvocationTargetException reflection error
   * @throws IllegalAccessException reflection error
   */
  public void writeToCSV(String path) throws IntrospectionException, IOException, InvocationTargetException, IllegalAccessException {
    new CSV<>(path, Patient.class, ";").write(patients, "firstName", "lastName",
        "socialSecurityNumber", "generalPractitioner", "diagnosis");
  }

  /**
   * Observable list of patients
   *
   * @return patients
   */
  public ObservableList<Patient> getPatients() {
    return patients;
  }

  /**
   * Close main stage and exit application
   *
   * @param event event
   */
  public void exit(Event event) {
    event.consume();
    primaryStage.close();
  }

  private final Consumer<Map<String, String>> updatePatientAction = fields -> {
    var isPresent = new AtomicBoolean(false);
    var id = fields.get("Social Security Number");
    var atomicPatient = new AtomicReference<Patient>();
    patients.stream().filter(p -> p.getSocialSecurityNumber().equals(id))
        .findAny().ifPresentOrElse(p -> {
      atomicPatient.set(p);
      patients.remove(p);
      isPresent.set(true);
    }, () -> atomicPatient.set(new Patient()));
    var patient = atomicPatient.get().updateFields(fields);

    try {
      patient.validate();
      patients.add(patient);
      LOGGER.log(INFO, () -> "%s patient: %s".formatted(
          !isPresent.get() ? "Added new" : "Updated", patient));
    } catch (Exception e) {
      DialogFactory.alert("Invalid data", Alert.AlertType.ERROR, e.getMessage());
      LOGGER.log(WARNING,
          () -> "Patient failed validation: %s".formatted(patient));
    }
  };

  private boolean patientExitst(Patient patient) {
    return patients.stream().anyMatch(p -> p.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber()));
  }

  /**
   * Add patient.
   *
   * @param patient patient to add
   */
  public void addPatient(Patient patient) {
    if (!patientExitst(patient)) {
      patients.add(patient);
    }
  }

  /**
   * Remove patient.
   *
   * @param patient patient to remove
   */
  public void removePatient(Patient patient) {
    patients.remove(patient);
  }

  /**
   * Get function run on patient update
   *
   * @return function
   */
  public Consumer<Map<String, String>> getUpdatePatientAction() {
    return updatePatientAction;
  }

  /**
   * Get main stage.
   *
   * @return main stage
   */
  public Stage getStage() {
    return primaryStage;
  }

  /**
   * Status property for displaying status messages.
   *
   * @return status property
   */
  public SimpleStringProperty getStatusProperty() {
    return statusProperty;
  }

  /**
   * Get selected patients.
   *
   * Should always contain one or zero patients.
   *
   * @return patients
   */
  public ObservableList<Patient> getSelectedPatients() {
    return patientTableView.getSelectionModel().getSelectedItems();
  }

}
