package no.jlangvand.idatt2001.patientregistry.app;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import no.jlangvand.idatt2001.patientregistry.controller.MainController;
import no.jlangvand.idatt2001.patientregistry.factory.DialogFactory;
import no.jlangvand.idatt2001.patientregistry.view.MainView;

import java.time.LocalDate;

public class App extends Application {

  public static final boolean GENERATE_DUMMY_DATA = true;
  public static final String VERSION = "v0.1.0";
  public static final String APP_NAME = "PatientRegistry";
  public static final String AUTHOR =
      "Joakim Skogø Langvand";
  public static final String EMAIL = "jlangvand@gmail.com";
  public static final String WEBPAGE = "jlangvand.no";
  public static final String ABOUT = """
      %s
            
      Copyright © %d
      %s <%s>
      %s
      Licensed under GPLv3
      """.formatted(VERSION, LocalDate.now().getYear(), AUTHOR,
      EMAIL, WEBPAGE);

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    Thread.setDefaultUncaughtExceptionHandler((t, e) ->
        DialogFactory.alert(e.getClass().getSimpleName(),
            Alert.AlertType.ERROR, e.getMessage(), e.toString()));
    var mainController = new MainController(primaryStage);
    var mainView = new MainView(mainController);
    mainView.show();
  }

}
