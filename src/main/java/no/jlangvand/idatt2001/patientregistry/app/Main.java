package no.jlangvand.idatt2001.patientregistry.app;

/**
 * Wrapper for JavaFX main class to workaround packaging issues.
 */
public class Main {

  public static void main(String[] args) {
    App.main(args);
  }

}
