package no.jlangvand.idatt2001.patientregistry.model;

import no.jlangvand.idatt2001.patientregistry.app.App;
import no.jlangvand.idatt2001.patientregistry.csv.Validatable;
import no.jlangvand.idatt2001.patientregistry.utility.SocialSecurityNumber;

import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class Patient implements Validatable {

  private static final Logger LOGGER =
      Logger.getLogger(Patient.class.getName());
  private String socialSecurityNumber;
  private String firstName;
  private String lastName;
  private String diagnosis;
  private String generalPractitioner;

  public String getSocialSecurityNumber() {
    return socialSecurityNumber;
  }

  public void setSocialSecurityNumber(String socialSecurityNumber) throws IllegalArgumentException {
    this.socialSecurityNumber = socialSecurityNumber;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getDiagnosis() {
    return diagnosis;
  }

  public void setDiagnosis(String diagnosis) {
    this.diagnosis = diagnosis;
  }

  public String getGeneralPractitioner() {
    return generalPractitioner;
  }

  public void setGeneralPractitioner(String generalPractitioner) {
    this.generalPractitioner = generalPractitioner;
  }

  public String getParamByHumanName(String name) {
    return switch (name) {
      case "First Name" -> firstName;
      case "Last Name" -> lastName;
      case "Social Security Number" -> socialSecurityNumber;
      case "Diagnosis" -> diagnosis;
      case "General Practitioner" -> generalPractitioner;
      default -> "";
    };
  }

  @Override
  public boolean validate() {
    return validate(App.GENERATE_DUMMY_DATA);
  }

  public boolean validate(boolean dummy) {
    /*
     * FOR THE SAKE OF TESTING ONLY:
     *
     * Check if social security number is present an valid, or generate a
     * dummy number if not.
     */
    if (dummy) {
      try {
        SocialSecurityNumber.validate(socialSecurityNumber);
      } catch (Exception e) {
        var oldNumber = socialSecurityNumber;
        socialSecurityNumber = SocialSecurityNumber.generateValidNumber(
            "010100");
        LOGGER.log(INFO,
            ("Invalid social security number: %s. error: \"%s\"," +
                "generating a dummy: %s")
                .formatted(oldNumber, e.getMessage(), socialSecurityNumber));
      }
    }
    if (firstName == null || firstName.isBlank() || lastName == null
        || lastName.isBlank()) throw new IllegalArgumentException(
        "First and last name is required");
    return SocialSecurityNumber.validate(socialSecurityNumber);
  }

  protected void setFields(Map<String, String> fields) {
    fields.forEach((k, v) -> {
      if (!v.isEmpty()) {
        switch (k) {
          case "First Name" -> setFirstName(v);
          case "Last Name" -> setLastName(v);
          case "Social Security Number" -> setSocialSecurityNumber(v);
          case "General Practitioner" -> setGeneralPractitioner(v);
          case "Diagnosis" -> setDiagnosis(v);
          default -> LOGGER.log(WARNING,
              () -> "Invalid field name: %s".formatted(v));
        }
      }
    });
  }

  public Patient updateFields(Map<String, String> fields) {
    var test = new Patient();
    test.setSocialSecurityNumber(this.socialSecurityNumber);
    test.setFields(fields);
    if (!test.validate(false)) return null;
    setFields(fields);
    return this;
  }

  @Override
  public String toString() {
    return ("Patient{socialSecurityNumber='%s', firstName='%s', " +
        "lastName='%s', diagnosis='%s', generalPractitioner='%s'}")
        .formatted(socialSecurityNumber, firstName, lastName, diagnosis,
            generalPractitioner);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    var patient = (Patient) o;
    return getSocialSecurityNumber().equals(patient.getSocialSecurityNumber());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getSocialSecurityNumber());
  }

}
