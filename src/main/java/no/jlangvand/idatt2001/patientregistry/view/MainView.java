package no.jlangvand.idatt2001.patientregistry.view;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import no.jlangvand.idatt2001.patientregistry.app.App;
import no.jlangvand.idatt2001.patientregistry.controller.MainController;
import no.jlangvand.idatt2001.patientregistry.factory.ButtonFactory;
import no.jlangvand.idatt2001.patientregistry.factory.DialogFactory;
import no.jlangvand.idatt2001.patientregistry.factory.FormFactory;
import no.jlangvand.idatt2001.patientregistry.factory.MenuBarFactory;
import no.jlangvand.idatt2001.patientregistry.factory.ToolbarFactory;
import no.jlangvand.idatt2001.patientregistry.model.Patient;
import no.jlangvand.idatt2001.patientregistry.utility.NamedActionHandler;
import no.jlangvand.idatt2001.patientregistry.utility.StringUtil;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import static no.jlangvand.idatt2001.patientregistry.controller.MainController.PATIENT_FIELDS;

public class MainView {

  private final MainController mainController;
  private final FormFactory formFactory;
  private final ObservableList<Patient> selectedPatients;

  private final EventHandler<ActionEvent> addPatientAction;
  private final EventHandler<ActionEvent> editPatientAction;
  private final EventHandler<ActionEvent> deletePatientAction;

  private MenuBar createMenuBar() {
    var menuBarFactory = new MenuBarFactory();

    EventHandler<ActionEvent> openCSV = event -> DialogFactory.csvFileChooser(mainController::openFile,
        DialogFactory.FileOperation.OPEN,
        mainController.getStage());

    EventHandler<ActionEvent> saveCSV = event -> {
      try {
        mainController.exportCSV(new FileChooser().showSaveDialog(mainController.getStage()));
      } catch (IntrospectionException | IOException | IllegalAccessException | InvocationTargetException e) {
        e.printStackTrace();
      }
    };

    menuBarFactory.createMenu("File",
        new NamedActionHandler("Import CSV", openCSV),
        new NamedActionHandler("Export CSV", saveCSV),
        new NamedActionHandler("Exit", mainController::exit)
    );

    menuBarFactory.createMenu("Edit",
        new NamedActionHandler("Add patient", addPatientAction),
        new NamedActionHandler("Edit selected patient", editPatientAction),
        new NamedActionHandler("Delete selected patient", deletePatientAction)
    );

    menuBarFactory.createMenu("Help",
        new NamedActionHandler("About",
            event -> DialogFactory.alert("About", App.ABOUT)));

    return menuBarFactory.getMenuBar();
  }

  private TableView<Patient> createPatientTable() {
    var tableView = new TableView<Patient>();
    PATIENT_FIELDS.forEach(label -> {
      var column = new TableColumn<Patient, String>(label);
      column.setCellValueFactory(
          new PropertyValueFactory<>(StringUtil.toPascalCase(label)));
      tableView.getColumns().add(column);
    });
    tableView.setItems(mainController.getPatients());
    tableView.setPrefWidth(800);
    return tableView;
  }

  public MainView(MainController mainController) {
    this.mainController = mainController;
    this.formFactory = new FormFactory(mainController);
    this.addPatientAction = event -> formFactory.showAddPatientForm();
    var patientsTable = createPatientTable();
    this.selectedPatients = patientsTable.getSelectionModel()
        .getSelectedItems();
    this.editPatientAction = event -> selectedPatients.stream().findFirst()
        .ifPresent(formFactory::showEditPatientForm);
    this.deletePatientAction = event -> selectedPatients.stream().findFirst()
        .ifPresent(patient ->
            DialogFactory.confirm("Delete patient?", patient.toString(),
                t -> mainController.removePatient(patient)));

    var borderPane = new BorderPane();

    var topBox = new VBox();
    var buttonFactory = new ButtonFactory(mainController);
    topBox.getChildren().addAll(
        createMenuBar(),
        ToolbarFactory.getToolbar(
            ButtonFactory.getButton(
                "Add Patient", addPatientAction),
            buttonFactory.getConditionalButton(
                "Edit patient", editPatientAction),
            buttonFactory.getConditionalButton(
                "Delete patient", deletePatientAction))
    );
    borderPane.setTop(topBox);

    borderPane.setCenter(patientsTable);

    var statusPane = new AnchorPane();
    var statusLabel = new Label();
    statusLabel.setPadding(new Insets(3,3,3,3));
    statusPane.getChildren().add(statusLabel);
    mainController.getStatusProperty().addListener(
        (v, s, t1) -> statusLabel.setText(t1));

    borderPane.setBottom(statusPane);

    var scene = new Scene(borderPane);
    mainController.getStage().setScene(scene);
  }

  public void show() {
    mainController.getStage().show();
  }

}
