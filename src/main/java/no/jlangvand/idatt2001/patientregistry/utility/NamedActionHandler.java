package no.jlangvand.idatt2001.patientregistry.utility;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class NamedActionHandler extends Tuple<String, EventHandler<ActionEvent>> {

  public NamedActionHandler(String x, EventHandler<ActionEvent> y) {
    super(x, y);
  }

}
