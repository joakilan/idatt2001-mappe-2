package no.jlangvand.idatt2001.patientregistry.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

import static java.util.logging.Level.FINE;

public class SocialSecurityNumber {
private static final Logger LOGGER =
    Logger.getLogger(SocialSecurityNumber.class.getName());
  private static final Random rng;

  static {
    rng = new Random();
  }

  private SocialSecurityNumber() {
    // Hide implicit constructor
  }

  public static boolean validate(String str) throws DateTimeException,
      NumberFormatException {
    if (!str.matches("\\d{11}")) {
      throw new IllegalArgumentException(
          "Social Security Number must be 11 digits");
    }
    var formatter = new SimpleDateFormat("ddMMyy");
    try {
      formatter.parse(str);
    } catch (ParseException e) {
      try {
        formatter.parse(str.replaceFirst(
            String.valueOf(str.charAt(0)),
            String.valueOf(
                Integer.parseInt(String.valueOf(str.charAt(0))) - 4)));
      } catch (ParseException e1) {
        throw new DateTimeException("Invalid date of birth");
      }
    }
    if (!validateControlNumbers(str)) {
      throw new NumberFormatException(
          "Invalid control numbers in social security number");
    }
    return true;
  }

  private static int[] calculateControlNumbers(String str) {
    var ints = strToIntegerList(str);
    var control1 =
        11 - ((3 * ints.get(0) + 7 * ints.get(1) + 6 * ints.get(2) + ints.get(3) + 8 * ints.get(4) + 9 * ints.get(5)
            + 4 * ints.get(6) + 5 * ints.get(7) + 2 * ints.get(8)) % 11);
    var control2 =
        11 - ((5 * ints.get(0) + 4 * ints.get(1) + 3 * ints.get(2) + 2 * ints.get(3) + 7 * ints.get(4) + 6 * ints.get(5)
            + 5 * ints.get(6) + 4 * ints.get(7) + 3 * ints.get(8) + 2 * control1) % 11);
    return new int[]{control1, control2};
  }

  private static ArrayList<Integer> strToIntegerList(String str) {
    var ints = new ArrayList<Integer>();
    Arrays.stream(str.split("")).forEach(c -> ints.add(Integer.parseInt(c)));
    return ints;
  }

  private static boolean validateControlNumbers(String str) {
    var ints = strToIntegerList(str);
    var control = calculateControlNumbers(str);
    return ints.get(9) == control[0] && ints.get(10) == control[1];
  }

  public static String generateValidNumber(String dob) {
    try {
      dob = dob.substring(0, 6);
      new SimpleDateFormat("ddMMyy").parse(dob);
    } catch (ParseException e) {
      throw new DateTimeException("Invalid date of birth");
    }
    var ints = new ArrayList<Integer>();
    var str = "";

    var control = new int[]{11, 11};
    while (control[0] > 9 || control[1] > 9) {
      ints.clear();
      Arrays.stream(dob.split("")).forEach(c -> ints.add(Integer.parseInt(c)));
      for (var i = 0; i < 3; i++) {
        var j = rng.nextInt(9);
        ints.add(Math.max(j, 1));
      }
        str = "";
        for (var k : ints) {
          str = "%s%d".formatted(str, k);
        }
        control = calculateControlNumbers(str);
    }
    str = "%s%d%d".formatted(str, control[0], control[1]);
    LOGGER.log(FINE, "Generated valid number: {0}",  str);
    return str;
  }

}
