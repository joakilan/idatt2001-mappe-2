package no.jlangvand.idatt2001.patientregistry.utility;

public class Tuple<T, U> {

  public final T x;
  public final U y;

  public Tuple(T x, U y) {
    this.x = x;
    this.y = y;
  }

}
