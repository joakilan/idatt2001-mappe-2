package no.jlangvand.idatt2001.patientregistry.utility;

public class StringUtil {

  private StringUtil() {
    // Hide implicit constructor
  }

  public static String toPascalCase(String str) {
    return str.replace(str.substring(0, 1), str.substring(0, 1).toLowerCase())
        .replace(" ", "");
  }

}
