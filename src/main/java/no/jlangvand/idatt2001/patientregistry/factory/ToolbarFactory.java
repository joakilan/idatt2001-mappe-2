package no.jlangvand.idatt2001.patientregistry.factory;

import javafx.scene.Node;
import javafx.scene.control.ToolBar;

public class ToolbarFactory {

  private ToolbarFactory() {
    // Hide implicit constructor
  }

  public static ToolBar getToolbar(Node... nodes) {
    var toolbar = new ToolBar();
    toolbar.getItems().addAll(nodes);
    return toolbar;
  }

}
