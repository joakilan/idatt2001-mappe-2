package no.jlangvand.idatt2001.patientregistry.factory;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import no.jlangvand.idatt2001.patientregistry.utility.NamedActionHandler;

import java.util.Arrays;
import java.util.function.Consumer;

public class MenuBarFactory {

  private final MenuBar menuBar = new MenuBar();

  public void createMenu(String name,
                         NamedActionHandler... items) {
    var menu = new Menu(name);
    Consumer<NamedActionHandler> createMenuItem =
        handler -> {
          var item = new MenuItem(handler.x);
          item.setOnAction(handler.y);
          menu.getItems().add(item);
        };

    Arrays.stream(items).forEach(createMenuItem);
    menuBar.getMenus().add(menu);
  }

  public MenuBar getMenuBar() {
    return menuBar;
  }
}
