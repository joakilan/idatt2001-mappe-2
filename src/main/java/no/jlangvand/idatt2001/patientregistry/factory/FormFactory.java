package no.jlangvand.idatt2001.patientregistry.factory;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import no.jlangvand.idatt2001.patientregistry.controller.MainController;
import no.jlangvand.idatt2001.patientregistry.model.Patient;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static javafx.scene.control.Alert.AlertType.ERROR;
import static javafx.stage.Modality.APPLICATION_MODAL;

public class FormFactory {

  private final MainController mainController;

  public FormFactory(MainController mainController) {
    this.mainController = mainController;
  }

  private static final Map<String, TextField> formFields;

  static {
    var socialSecurityField = new TextField();
    formFields = new LinkedHashMap<>(Map.of(
        "Social Security Number", socialSecurityField,
        "First Name", new TextField(),
        "Last Name", new TextField(),
        "General Practitioner", new TextField(),
        "Diagnosis", new TextField()
    ));
  }

  public void showAddPatientForm() {
    showPatientForm(new Patient(), "Add Patient");
  }

  public void showEditPatientForm(Patient patient) {
    showPatientForm(patient, "Edit Patient");
  }

  private void showPatientForm(Patient patient, String title) {
    var stage = new Stage();

    var gridPane = new GridPane();
    gridPane.setHgap(10);
    gridPane.setPadding(new Insets(10, 10, 10, 10));
    formFields.forEach((k, v) -> {
      var label = new Label(k);
      label.setLabelFor(v);
      label.setPrefHeight(30);
      v.setText(patient.getParamByHumanName(k));
      GridPane.setColumnIndex(label, 0);
      GridPane.setColumnIndex(v, 1);
      GridPane.setRowIndex(label, gridPane.getRowCount());
      GridPane.setRowIndex(v, gridPane.getRowCount());
      gridPane.getChildren().addAll(label, v);
    });

    var titlePane = new AnchorPane();
    var titleText = new Label();
    titlePane.setPadding(new Insets(10,10,0,10));
    titleText.setFont(new Font(18));
    titleText.setText(title);
    titlePane.getChildren().add(titleText);

    var cancelButton = new Button("Cancel");
    cancelButton.setOnAction(event -> stage.close());
    var saveButton = new Button("Save");
    saveButton.setOnAction(event -> {
      var stringMap = new HashMap<String, String>();
      formFields.forEach((k, v) -> stringMap.put(k, v.getText()));
      if (patient.updateFields(stringMap) == null) {
        DialogFactory.alert("Invalid data", ERROR, "Validation failed");
      } else {
        mainController.addPatient(patient);
        stage.close();
      }
    });
    var buttonBar = new ButtonBar();
    buttonBar.setPadding(new Insets(10, 10, 10, 10));
    buttonBar.getButtons().addAll(cancelButton, saveButton);

    var pane = new BorderPane();
    pane.setTop(titlePane);
    pane.setCenter(gridPane);
    pane.setBottom(buttonBar);

    var scene = new Scene(pane);
    stage.setScene(scene);
    stage.initOwner(mainController.getStage());
    stage.initModality(APPLICATION_MODAL);
    stage.showAndWait();
  }

}
