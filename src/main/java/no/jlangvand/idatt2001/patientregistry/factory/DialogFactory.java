package no.jlangvand.idatt2001.patientregistry.factory;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Optional;
import java.util.function.Consumer;

import static javafx.scene.control.Alert.AlertType.CONFIRMATION;
import static javafx.scene.control.Alert.AlertType.INFORMATION;

public class DialogFactory {

  private DialogFactory() {
    // Hide implicit constructor
  }

  public enum FileOperation {
    OPEN,
    SAVE
  }

  public static void csvFileChooser(Consumer<File> action,
                                    FileOperation operation, Stage stage) {
    var fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
        "Comma Separated Value File", "*.csv"));
    action.accept(operation == FileOperation.OPEN
        ? fileChooser.showOpenDialog(stage)
        : fileChooser.showSaveDialog(stage));
  }

  public static Optional<ButtonType> alert(String header, String info) {
    return alert(header, INFORMATION, info);
  }

  public static Optional<ButtonType> alert(String header, Alert.AlertType type, String info) {
    return alert(header, type, info, "");
  }

  public static Optional<ButtonType> alert(String header, Alert.AlertType type, String info,
                                           String extendedInfo) {
    var alert = new Alert(type, info);
    alert.setHeaderText(header);
    alert.setTitle(header);

    if (!extendedInfo.isEmpty()) {
      alert.getDialogPane().setExpandableContent(new Text(extendedInfo));
    }
    return alert.showAndWait();
  }

  public static void confirm(String header, String info,
                                            Consumer<ButtonType> onConfirmation) {
    var alert = new Alert(CONFIRMATION, info);
    alert.setHeaderText(header);
    alert.setTitle(header);

    alert.showAndWait().stream().filter(ButtonType.OK::equals).findFirst()
        .ifPresent(onConfirmation);
  }

}
