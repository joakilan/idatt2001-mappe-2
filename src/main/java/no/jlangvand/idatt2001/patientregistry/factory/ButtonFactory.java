package no.jlangvand.idatt2001.patientregistry.factory;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import no.jlangvand.idatt2001.patientregistry.controller.MainController;
import no.jlangvand.idatt2001.patientregistry.model.Patient;

import java.util.function.BiFunction;

public class ButtonFactory {

  private final MainController mainController;

  public ButtonFactory(MainController mainController) {
    this.mainController = mainController;
  }

  private static final BiFunction<String, EventHandler<ActionEvent>, Button>
      createButton = (text, action) -> {
    var button = new Button(text);
    button.setOnAction(action);
    return button;
  };

  public static Button getButton(String text,
                                 EventHandler<ActionEvent> action) {
    return createButton.apply(text, action);
  }

  public Button getConditionalButton(String text,
                                     EventHandler<ActionEvent> action) {
    var button = getButton(text, action);
    button.setDisable(true);
    disableOnNoSelect(button,
        mainController.getPatients());
    return button;
  }

  public static Node disableOnNoSelect(Node node,
                                       ObservableList<Patient> list) {
    list.addListener((ListChangeListener.Change<? extends Patient> observable) -> node.disableProperty().set(list.isEmpty()));
    return node;
  }

  public static ButtonBar getButtonBar(Button... buttons) {
    var buttonBar = new ButtonBar();
    buttonBar.getButtons().addAll(buttons);
    return buttonBar;
  }

}
