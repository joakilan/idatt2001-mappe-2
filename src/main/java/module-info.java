module no.jlangvand.idatt2001.patientregistry {
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.graphics;
  requires java.desktop;
  requires java.logging;
  requires org.jetbrains.annotations;

  exports no.jlangvand.idatt2001.patientregistry.app;
  exports no.jlangvand.idatt2001.patientregistry.controller;
  exports no.jlangvand.idatt2001.patientregistry.factory;
  exports no.jlangvand.idatt2001.patientregistry.model;
  exports no.jlangvand.idatt2001.patientregistry.utility;
  exports no.jlangvand.idatt2001.patientregistry.view;

  opens no.jlangvand.idatt2001.patientregistry.controller to javafx.fxml;
  opens no.jlangvand.idatt2001.patientregistry.model to java.base;
  exports no.jlangvand.idatt2001.patientregistry.csv;
}